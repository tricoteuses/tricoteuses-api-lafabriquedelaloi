extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate toml;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Api {
    pub allowed_origins: Vec<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub api: Api,
    pub data: Data,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Data {
    pub dir: String,
}

#[derive(PartialEq)]
pub enum Verbosity {
    Verbosity0,
    Verbosity1,
    Verbosity2,
}

pub fn load(config_file_path: &Path) -> Config {
    let mut config_file = File::open(config_file_path).expect("Configuration file not found");
    let mut config_string = String::new();
    config_file
        .read_to_string(&mut config_string)
        .expect("Something went wrong reading the file");
    toml::from_str(&config_string).unwrap()
}
