extern crate clap;
extern crate rayon;
extern crate tricoteuses_api_lafabriquedelaloi_config as config;

use clap::{App, Arg};
use config::Verbosity;
use rayon::prelude::*;
use std::fs::{self, File};
use std::io::BufRead;
use std::path::Path;
use std::process::Command;

fn main() {
    let matches = App::new("Tricoteuses API LaFabriqueDeLaLoi.fr")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data from LaFabriqueDeLaLoi.fr")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();
    let data_dir = config_dir.join(config.data.dir);
    if !data_dir.exists() {
        fs::create_dir(&data_dir).expect("Creation of data directory failed");
    }

    let title = "Tous les députés";
    let url = "https://www.nosdeputes.fr/deputes/json";

    if verbosity != Verbosity::Verbosity0 {
        println!("Loading \"{}\"...", title);
    }

    let output = Command::new("wget")
        .current_dir(&data_dir)
        .arg("-q")
        .arg(&url)
        .output()
        .expect("Wget failed");
    if !output.status.success() {
        println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
        println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
        panic!("Wget failed");
    }

    let downloaded_json_filename = "json";
    let json_file_path = data_dir.join("deputes.json");
    let json_file = File::create(&json_file_path).unwrap();
    let output = Command::new("jq")
        .current_dir(&data_dir)
        .arg(".")
        .arg(&downloaded_json_filename)
        .stdout(json_file)
        .output()
        .expect("Jq reformat failed");
    if !output.status.success() {
        println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
        panic!("Jq reformat failed");
    }
    fs::remove_file(data_dir.join(&downloaded_json_filename))
        .expect("Donwloaded JSON file removal failed");

    if verbosity != Verbosity::Verbosity0 {
        println!("Loaded \"{}\".", title);
    }

    let output = Command::new("jq")
        .current_dir(&data_dir)
        .arg("-r")
        .arg(".deputes[] | .depute.url_nosdeputes_api")
        .arg(&json_file_path)
        .output()
        .expect("Jq URLs extraction failed");
    if !output.status.success() {
        println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
        println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
        panic!("Jq URLs extraction failed");
    }

    let deputes_dir = data_dir.join("deputes");
    if !deputes_dir.exists() {
        fs::create_dir(&deputes_dir).expect("Creation of deputes JSON directory failed");
    }
    let deputes_urls: Vec<String> = output.stdout.lines().map(|line| line.unwrap()).collect();
    deputes_urls.par_iter().for_each(|depute_url| {
        let depute_slug = depute_url.split('/').nth(3).unwrap();
        if verbosity != Verbosity::Verbosity0 {
            println!("Loading depute {} JSON from {}...", depute_slug, depute_url);
        }

        let downloaded_json_filename = format!("{}_unformatted.json", depute_slug);
        let output = Command::new("wget")
            .current_dir(&deputes_dir)
            .arg("-q")
            .arg("--output-document")
            .arg(&downloaded_json_filename)
            .arg(&depute_url)
            .output()
            .expect("Wget failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Wget failed");
        }

        let json_filename = format!("{}.json", depute_slug);
        let json_file_path = deputes_dir.join(json_filename);
        let json_file = File::create(&json_file_path).unwrap();
        let output = Command::new("jq")
            .current_dir(&deputes_dir)
            .arg(".")
            .arg(&downloaded_json_filename)
            .stdout(json_file)
            .output()
            .expect("Jq reformat failed");
        if !output.status.success() {
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Jq reformat failed");
        }
        fs::remove_file(deputes_dir.join(&downloaded_json_filename))
            .expect("Donwloaded JSON file removal failed");
    });
}
