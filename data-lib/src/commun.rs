// #[derive(Clone, Debug, Deserialize)]
// #[serde(deny_unknown_fields)]
// pub struct EmptyChoice {}

// graphql_scalar!(EmptyChoice {
//     resolve(&self) -> juniper::Value {
//         juniper::Value::Null
//     }

//     from_input_value(v: &InputValue) -> Option<EmptyChoice> {
//         None
//     }

//     from_str<'a>(value: ScalarToken<'a>) -> juniper::ParseScalarResult<'a, juniper::DefaultScalarValue> {
//         Err(juniper::parser::ParseError::UnexpectedToken(juniper::parser::Token::Scalar(value)))
//     }
// });

pub type EmptyChoice = bool;
