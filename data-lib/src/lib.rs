extern crate juniper;
#[macro_use]
extern crate juniper_codegen;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tricoteuses_api_lafabriquedelaloi_config as config;

mod commun;
mod contexts;
mod deputes;

use std::collections::HashMap;
use std::fs::{self, File};
use std::path::Path;

use crate::config::{Config, Verbosity};
pub use crate::contexts::Context;
pub use crate::deputes::{Depute, DeputeWrapper};

pub struct EnabledDatasets {
    pub deputes: bool,
}

pub const ALL_DATASETS: EnabledDatasets = EnabledDatasets { deputes: true };

pub const NO_DATASET: EnabledDatasets = EnabledDatasets { deputes: false };

pub fn load(
    config: &Config,
    config_dir: &Path,
    enabled_datasets: &EnabledDatasets,
    verbosity: &Verbosity,
) -> Context {
    let data_dir = config_dir.join(&config.data.dir);

    let mut deputes: Vec<Depute> = Vec::new();
    if enabled_datasets.deputes {
        let deputes_dir = data_dir.join("deputes");
        for entry in fs::read_dir(deputes_dir).unwrap() {
            let json_file_path = entry.unwrap().path();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"député\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let depute_wrapper: DeputeWrapper = serde_json::from_reader(json_file).unwrap();
            deputes.push(depute_wrapper.depute);
        }
    }

    let mut depute_by_id_an: HashMap<String, *const Depute> = HashMap::new();
    for depute in &deputes {
        depute_by_id_an.insert(depute.id_an.clone(), depute);
    }

    Context {
        depute_by_id_an,
        deputes,
    }
}
