use juniper;
use std::collections::HashMap;

use crate::deputes::Depute;

pub struct Context {
    pub depute_by_id_an: HashMap<String, *const Depute>,
    pub deputes: Vec<Depute>,
}

impl Context {
    pub fn get_depute_at_id_an(&self, id_an: &str) -> Option<&Depute> {
        self.depute_by_id_an
            .get(&id_an.to_string())
            .map(|depute| unsafe { &*(*depute) })
    }
}

unsafe impl Send for Context {}
unsafe impl Sync for Context {}
impl juniper::Context for Context {}
