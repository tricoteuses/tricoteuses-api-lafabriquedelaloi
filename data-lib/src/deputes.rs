use crate::commun::EmptyChoice;

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct AdresseWrapper {
    pub adresse: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct AncienMandatWrapper {
    pub mandat: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct AutreMandatWrapper {
    pub mandat: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct CollaborateurWrapper {
    pub collaborateur: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Depute {
    pub adresses: Vec<AdresseWrapper>,
    #[serde(default)]
    pub ancien_depute: i32, // TODO: Replace to bool (always 1 when present).
    anciens_autres_mandats: Vec<EmptyChoice>,
    pub anciens_mandats: Vec<AncienMandatWrapper>,
    pub autres_mandats: Vec<AutreMandatWrapper>,
    pub collaborateurs: Vec<CollaborateurWrapper>,
    pub date_naissance: String,
    pub emails: Vec<EmailWrapper>,
    pub groupe: Option<Responsabilite>,
    pub groupes_parlementaires: Vec<ResponsabiliteWrapper>,
    pub groupe_sigle: String,
    pub id: i32,
    pub id_an: String,
    pub lieu_naissance: Option<String>,
    pub mandat_debut: String,
    pub mandat_fin: Option<String>,
    pub nb_mandats: i32,
    pub nom: String,
    pub nom_circo: String,
    pub nom_de_famille: String,
    pub num_circo: i32,
    pub num_deptmt: String,
    pub parti_ratt_financier: String,
    pub place_en_hemicycle: String,
    pub prenom: String,
    pub profession: Option<String>,
    pub responsabilites: Vec<ResponsabiliteWrapper>,
    pub responsabilites_extra_parlementaires: Vec<ResponsabiliteWrapper>,
    pub historique_responsabilites: Vec<HistoriqueResponsabiliteWrapper>,
    pub sexe: Sexe,
    pub sites_web: Vec<SiteWebWrapper>,
    pub slug: String,
    pub twitter: String,
    pub url_an: String,
    pub url_nosdeputes: String,
    pub url_nosdeputes_api: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DeputeWrapper {
    pub depute: Depute,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct EmailWrapper {
    pub email: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct HistoriqueResponsabilite {
    pub debut_fonction: String,
    pub fin_fonction: String,
    pub fonction: String,
    pub organisme: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct HistoriqueResponsabiliteWrapper {
    pub responsabilite: HistoriqueResponsabilite,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Responsabilite {
    pub debut_fonction: String,
    pub fonction: String,
    pub organisme: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ResponsabiliteWrapper {
    pub responsabilite: Responsabilite,
}

#[derive(Debug, Deserialize, GraphQLEnum)]
pub enum Sexe {
    #[graphql(name = "F")]
    #[serde(rename = "F")]
    Femme,
    #[graphql(name = "H")]
    #[serde(rename = "H")]
    Homme,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SiteWebWrapper {
    pub site: String,
}
