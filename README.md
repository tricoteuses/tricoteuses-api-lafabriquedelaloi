# Tricoteuses-API-LaFabriqueDeLaLoi

## _API GraphQL pour Tricoteuses basée sur les données open data de NosDeputes.fr, NosSenateurs.fr et LaFabriqueDeLaLoi.fr_

## Utilisation

### Récupération quotidienne des données

Ce script récupère différents fichiers JSON des sites [NosDeputes.fr](https://www.nosdeputes.fr), [NosSenateurs.fr](https://www.nossenateurs.fr) et [LaFabriqueDeLaLoi.fr](https://www.lafabriquedelaloi.fr). Il les réindente (car par défaut ils tiennent sur une ligne, ce qui ne facilite pas le debogage) :

```bash
cargo run -p tricoteuses_api_lafabriquedelaloi_fetcher -- -c Config.toml -v
```

### Lancement du serveur web

La commande ci-dessous, charge en mémoire tous les fichiers JSON précédentes et lance un serveur web proposant une API GraphQL :

```bash
cargo run -p tricoteuses_api_lafabriquedelaloi -- -c Config.toml -v
```

Pour tester le service GraphQL avec l'interface GraphiQL, ouvrir l'URL `http://localhost:8001` dans un navigateur.
